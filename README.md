# README #

First programming task for a course Introduction to Programming Systems:
Program transfer a text from input to output that each sequence of white characters replaces by a space.
Program can divide a text to required number of words or sentences to one line by using --wpl and --spl parameters.

Compile project by command
```
#!c

make
```