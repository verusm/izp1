/*
* Nazev souboru: proj1.c
* Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz
* Datum: 31. 10. 2009
* Projekt: Zpracovani textu, projekt cislo 1 pro predmet IZP
* Popis programu: Program prevede zadany text ze vstupu na vystup tak, ze veskerou posloupnost
*               bilych znaku nahradi jednou mezerou. Dale podle zadanych parametru program umi
*               rozdelit text na urcity pocet slov nebo vet na radek.
*/

/** Knihovny potrebne pro beh programu.**/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

/** Konstanty pouzivane k rozliseni stavu, ve kterem se nachazim. **/
const int START = 1;
const int SLOVO = 2;
const int MEZERA = 3;

/** Funkce, ktera nam po zavolani vypise napovedu pro tento program.**/
void napoveda(void)
{
    printf("Program Zpracovani textu (2009)\n"
          "Autor: Vera Mullerova \n"
          "**************************** \n"
          "Program prevede text ze standardniho vstupu na standardni vystup tak, \n"
          "ze posloupnost bilych znaku (mezery, tabulatory, atd.) nahradi pouze jednou mezerou. \n"
          "Dale program odstrani vsechny bile znaky na zacatku vstupu a za poslednim nebilym znakem. \n"
          "Funkce programu jsou dale rozsireny o nasledujici parametry. \n \n"
          "Popis parametru: \n"
          "     -h ....  zobrazi tuto napovedu \n"
          "     --wpl N..oddeli vstup po N slovech na radek (N je cele cislo)\n"
          "     --spl N..oddeli vstup po N vetach na radek (N je cele cislo)\n"
    );
}

/** Vyctovy typ chyby, ve kterem jsou definovane chyby, ktere se mohou v programu objevit. **/
enum chyby
{
    EROK, // bez chyby
    ERRPARAM, // spatny parametr
    ERZNAK, // neznamy znak
    ERPOCET, // maly nebo velky pocet slov nebo vet na radek - N je male nebo zaporne nebo doslo k preteceni
    ERCISLO, // N neni cislo
    ERNEVIM // neznama chyba

};

/** Definice pole s chybovymi hlaskami. **/
const char *CHYBY[] =
{
    "Vse v poradku.",
    "Spatny parametr.",
    "Spatny znak.",
    "Spatne zadany pocet slov nebo vet na radek!\nZadali jste moc male nebo moc velke cislo.",
    "Je treba zadat pouze ciselny parametr.",
    "Neznama chyba."
};

/**
* Funkce vypisujici chybove hlasky.
* @param chyba Chyba z vyctu chyb.
**/
void vypisChybu(int chyba)
{
    if (chyba < EROK || chyba >= ERNEVIM) //pokud neni chybova hlaska mezi EROK a ERNEVIM, tak se stalo neco spatne
        chyba = ERNEVIM; //a vypise se mi chybova hlaska ERNEVIM
    fprintf(stderr, "%s \n", CHYBY[chyba]); //jinak se vypise prislusna chyba, ktera v programu opravdu nastala
}

/**
* Funkce, ktera vypise znak a vrati konstantu SLOVO.
* @param znak Znak ziskany ze standardniho vstupu.
**/
int vratSlovo (int znak)
{
    putchar(znak);
    return SLOVO;
}

/**
* Funkce, ktera se provede pokud budu ve stavu START.
* Zjisti, zda je predavany znak bilym znakem a pokud ano,
* tak se nic neprovede.
* Pokud predavany znak neni bilym znakem, zavola se funkce vratSlovo.
* @param znak Znak ziskany ze standardniho vstupu.
**/
int start(int znak)
{
    if ((isspace(znak)) == 0) // znak neni bily znak
        return vratSlovo(znak);
    else if ((isspace(znak)) != 0) // znak je bilym znakem
        return START;
    else return ERZNAK; // vrati chybu
}

/**
* Funkce, ktera se provede pokud budu ve stavu SLOVO.
* Zjisti, zda je predavany znak bilym znakem a pokud ano,
* tak se stav zmeni na MEZERA.
* Pokud predavany znak neni bilym znakem, tak se vypise a stav zustava stejny.
* @param znak Znak ziskany ze standardniho vstupu.
* @param pravda Pokud je nastaven na 0, znamena to,
* ze uz mame potrebny pocet slov nebo vet na radku a musime odentrovat. Jinak pokracujeme dale.
**/
int slovo(int znak, int pravda)
{
    if ((isspace(znak)) == 0) // predavany znak neni bilym znakem
    {
        if (pravda == 0) //zjistuji, zda uz je dost slov ci vet na radek
            {
                putchar('\n');
                return vratSlovo(znak);
            }
        else if (pravda == 1)
            return vratSlovo(znak);
        else return ERNEVIM; // vrati neznamou chybu
    }
    else if ((isspace(znak)) != 0) // znak je bilym znakem
        return MEZERA;
    else return ERZNAK; // vrati chybu
}

/**
* Funkce, ktera se provede pokud budu ve stavu MEZERA.
* Zjisti, zda je predavany znak bilym znakem a pokud ano, tak se nic nestane.
* Pokud predavany znak neni bilym znakem,
* tak se vypise mezera a pak samotny znak a nakonec se stav zmeni na SLOVO.
* @param znak Znak ziskany ze standardniho vstupu.
* @param pravda Pokud je nastaven na 0, znamena to,
* ze uz mame potrebny pocet slov nebo vet na radku a musime odentrovat. Jinak pokracujeme dale.
**/
int mezera(int znak, int pravda)
{
    if ((isspace(znak)) == 0) // predavany znak neni bilym znakem
    {
        if (pravda == 0) //zjistuji, zda uz je dost slov ci vet na radek
        {
            putchar('\n');
            return vratSlovo(znak);
        }
        else if (pravda == 1)
        {
            putchar(' ');
            return vratSlovo(znak);
        }
        else return ERNEVIM; // vrati neznamou chybu
    }
    else if ((isspace(znak)) != 0) // znak je bilym znakem
        return MEZERA;
    else return ERZNAK; // vrati chybu
}

/**
* Funkce, ktera kontroluje, zda se zadany ciselny parametr vejde do promenne unsigned long int.
**/
int kontrola(char *argv[])
{
    char *chyba, *s;
    s = argv[2];
    int pocet = strtoul(s, &chyba, 10); // prevadim parametr na unsigned long int, abych zjistila,
                                        // zda jde o cislo a vejde se do promenne
    if (chyba == s) //uz 1. znak z 2. parametru nebylo cislo
        return ERCISLO;
    if (*chyba != '\n' && *chyba != 0) //nejaky dalsi znak z 2. parametru neni cislice
        return ERRPARAM;
    if (pocet <= 0) //overuji zda je parametr dost velky
        return ERPOCET;
    else
        return EROK; // pokud to projde az sem, znamena to, ze je vse v poradku
                     // a ciselny parametr se vejde do typu unsigned long int
}

/**
* Funkce, ktera prevadi standardni vstup na standardni vystup.
* Funkce se provede, pokud byl zadan parametr --spl N - N je cislo, ktere urcuje, kolik vet na radek se bude zobrazovat
* @param pocet Pocet vet, ktere se budou zobrazovat na jeden radek.
**/
int vetyNaRadek(int pocet)
{
    int ch, predch;
    int stav = START;
    int chyboveHlaseni = EROK;
    int vet = 0;
    int pravda = 1;
    while ((ch = getchar()) != EOF && chyboveHlaseni == EROK) //beru jednotlive znaky zapsane na standardni vstup
    {
        if (vet == pocet) // zjistuji, zda uz se dosahlo pozadovaneho poctu vet na radek
        {
            vet = 0;
            pravda = 0;
        }
        switch (stav) // udela se preklad (nahrazeni posloupnosti bilych znaku jednou mezerou) pomoci konecneho automatu
        {
            case 1: stav = start(ch); break;
            case 2: if (predch == 46 && ch != 46) // 46 je znak "."
                    // pokud je predchazejici znak tecka a ted uz tecka neni, tak se to zapocita jako dalsi veta
                        vet++;
                    if (vet == pocet) // opet zjistuji, zda uz se dosahlo pozadovaneho poctu vet na radek
                    {
                        vet = 0;
                        pravda = 0;
                    }
                    stav = slovo(ch, pravda);
                    if (stav == SLOVO)
                        pravda = 1; // nastavuji, abych vedela, ze jeste nebylo dosazeno pozadovaneho poctu vet
                    break;
            case 3: stav = mezera(ch, pravda);
                    if (stav == SLOVO)
                        pravda = 1; // nastavuji, abych vedela, ze jeste nebylo dosazeno pozadovaneho poctu vet
                    break;
            default: return ERZNAK; break; // vrati chybu hlavni funkci
        };
        if (stav != 1 && stav != 2 && stav != 3) // pokud vse probehlo bez chyby, mohu pokracovat dal
            chyboveHlaseni = ERNEVIM;
        predch = ch;
    };
    return chyboveHlaseni; // vrati se rizeni hlavni funkci predanim chyboveho hlaseni
}

/**
* Funkce, ktera prevadi standardni vstup na standardni vystup.
* Funkce se provede, pokud byl zadan parametr --wpl N - N je cislo, ktere urcuje, kolik slov na radek se bude zobrazovat
* @param pocet Pocet slov, ktere se budou zobrazovat na jeden radek.
**/
int slovaNaRadek(int pocet)
{
    int ch;
    int stav = START;
    int chyboveHlaseni = EROK;
    int slov = 0;
    int pravda = 1;

    while ((ch = getchar()) != EOF && chyboveHlaseni == EROK) //beru jednotlive znaky zapsane na standardni vstup
    {
        if (slov == pocet)
        {
            slov = 0;
            pravda = 0;
        }
        switch (stav) // udela se preklad (nahrazeni posloupnosti bilych znaku jednou mezerou) pomoci konecneho automatu
        {
            case 1: stav = start(ch); break;
            case 2: stav = slovo(ch, pravda);
                    if (stav == MEZERA)
                        slov++;
                    break;
            case 3: stav = mezera(ch, pravda);
                    if (stav == SLOVO)
                        pravda = 1;
                    break;
            default: return ERZNAK; break; // vrati chybu hlavni funkci
        };
        if (stav != 1 && stav != 2 && stav != 3) // pokud vse probehlo bez chyby, mohu pokracovat dal
            chyboveHlaseni = ERNEVIM;
    }
    return chyboveHlaseni; // vrati se rizeni hlavni funkci predanim chyboveho hlaseni
}

/**
* Funkce se prelozi standardni vstup na standardni vystup.
* Funkce se provede pouze pokud spustime program bez parametru.
**/
int preklad()
{
    int ch;
    int stav = START;
    int chyboveHlaseni = EROK;
    int pravda = 1;
    while ((ch = getchar()) != EOF && chyboveHlaseni == EROK) //beru jednotlive znaky zapsane na standardni vstup
    {
        switch (stav) // udela se preklad (nahrazeni posloupnosti bilych znaku jednou mezerou) pomoci konecneho automatu
        {
            case 1: stav = start(ch); break;
            case 2: stav = slovo(ch, pravda); break;
            case 3: stav = mezera(ch, pravda); break;
            default: return ERZNAK; break; // vrati chybu hlavni funkci
        };
        if (stav != 1 && stav != 2 && stav != 3) // pokud vse probehlo bez chyby, mohu pokracovat dal
            chyboveHlaseni = ERNEVIM;
    };
    return chyboveHlaseni; // vrati se rizeni hlavni funkci predanim chyboveho hlaseni
}

/**
* Funkce, ktera nam vrati cislo podle toho, ktery parametr uzivatel zadal.
* 1 - parametr -h
* 2 - bez parametru
* 3 - parametr --wpl
* 4 - parametr --spl
* 5 - chybny 1. parametr
* @param argc Pocet argumentu.
* @param argv Pole textovych retezcu s argumenty.
*/
int zjistiParam(int argc, char *argv[])
{
    if (argc == 2 && strcmp("-h", argv[1]) == 0)
        return 1;
    else if (argc == 1)
        return 2;
    else if (argc == 3 && strcmp("--wpl", argv[1]) == 0)
        return 3;
    else if (argc == 3 && strcmp("--spl", argv[1]) == 0)
        return 4;
    else return 5;
}

/**
* Hlavni funkce.
* @param argc Pocet argumentu.
* @param argv Pole textovych retezcu s argumenty.
**/
int main(int argc, char *argv[])
{
    int chyba = ERNEVIM;
    int param = zjistiParam(argc, argv); // do promenne param zapisu cislo podle toho, ktery parametr uzivatel zadal

    switch (param) // podle toho jaky 1. parametr uzivatel zadal, pokracuji ve vykonavani programu
    {
        case 1: napoveda(); // parametr je -h
                chyba = EROK; break;
        case 2: chyba = preklad(); break; // bez parametru
        case 3: chyba = kontrola(argv); // parametr je --wpl N
                if (chyba == EROK)
                {
                    char *pomocna;
                    int pocet = strtoul(argv[2], &pomocna, 10); // do promenne pocet prevadim 2. parametr,
                                                                // o kterem uz vim, ze se vejde do promenne unsigned long int
                    chyba = slovaNaRadek(pocet);
                };
                break;
        case 4: chyba = kontrola(argv); // parametr je --spl N
                if (chyba == EROK)
                {
                    char *pomocna;
                    int pocet = strtoul(argv[2], &pomocna, 10); // do promenne pocet prevadim 2. parametr,
                                                                // o kterem uz vim, ze se vejde do promenne unsigned long int
                    chyba = vetyNaRadek(pocet);
                };
                break;
        case 5: chyba = ERRPARAM; break; // chybny parametr
        default: chyba = ERNEVIM; break; // neco neni v poradku
    }

    if (chyba != EROK) //kontrola chybove hlasky a pripadne jeji vypsani, pokud se behem programu nejaka chyba vyskytla
    {
        vypisChybu(chyba);
        return EXIT_FAILURE;
    }

    else return EXIT_SUCCESS;
}
